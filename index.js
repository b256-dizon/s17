/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

function printInfo(){
	let input1 = prompt ("Enter your Full name:"); 
	let input2 = prompt ("Enter your Age:"); 
	let input3 = prompt ("Enter your location:");

	console.log("Hello " + input1 +"."); 
	console.log("You are " + input2 + " years old."); 
	console.log("You live in " + input3 +"."); 
};

printInfo();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

function printArtist() {
	console.log("My Top 5 Artist");
	console.log("1. Moira");
	console.log("2. One Direction"); 
	console.log("3. December Avenue"); 
	console.log("4. FM Static"); 
	console.log("5. Sam Mangubat"); 
}

printArtist	();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

function printMovies() {
	console.log("My Top 5 Movies");
	console.log("1. Interstellar");
	console.log("Rotten Tomato Rating: " + "86%");
	console.log("2. She's Dating the Gangster"); 
	console.log("Rotten Tomato Rating: " + "76%");
	console.log("3. Up");
	console.log("Rotten Tomato Rating: " + "90%" );
	console.log("4. Big Hero 6");
	console.log("Rotten Tomato Rating: " + "91%");
	console.log("5. Cars"); 
	console.log("Rotten Tomato Rating: " + "79%");
}

	printMovies();





/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt ("Enter your first friend's name:"); 
	let friend2 = prompt ("Enter your second friend's name:"); 
	let friend3 = prompt ("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printUsers();

